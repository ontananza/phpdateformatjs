var PHPDateFormat = require('./src/PHPDateFormat');

console.log('string date instantiation var my_date = new PHPDateFormat("2017-02-25 09:55","es")');

var my_date = new PHPDateFormat("2017-02-25 09:55","es");

console.log('format (es) "t"',my_date.format("%t"));
console.log('format (es) "L"',my_date.format("%L"));
console.log('format (es) "w"',my_date.format("%w"));
console.log('format (es) "W"',my_date.format("%W"));
console.log('format (es) "z"',my_date.format("%z"));
console.log('format (es) "d F y H:i A"',my_date.format("%l, %d %F %y %h:%i %A"));
console.log('format (en) "d F y H:i A"',my_date.format("%l, %d %F %y %h:%i %A","en"));

console.log('getDateISO8601',my_date.getDateISO8601());
console.log('getNumeric',my_date.getNumeric());

console.log('addMonths(1)',my_date.addMonths(1));
console.log('getDateISO8601',my_date.getDateISO8601());

console.log('addDays(7)',my_date.addDays(7));
console.log('getDateISO8601',my_date.getDateISO8601());

console.log('isDayBetween(6,0)',my_date.isDayBetween(5,6));
console.log('format (es) "d F y H:i A"',my_date.format("%l, %d %F %y %h:%i %A","es"));
console.log('format (en) "d F y H:i A"',my_date.format("%l, %d %F %y %h:%i %A","en"));

console.log('addDays(-1)',my_date.addDays(-1));
console.log('getDateISO8601',my_date.getDateISO8601());
