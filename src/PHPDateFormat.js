/*
 * JS date format using same PHP interface. Spanish / English
 * @author Julio González <ontananza@msn.com>
 * */
String.prototype.zeroLeftPad = function() {
	return String("00" + this).slice(-2);
};

Date.prototype.isValid = function() {
	return this.getTime() === this.getTime();
};

Date.prototype.addDays = function(days) {
	this.setDate(this.getDate() + parseInt(days));
	return this;
};

Date.prototype.getDaysOfMonth = function() {

	var d = new Date(this.getFullYear(), this.getMonth() + 1, 0);
	return d.getDate();
};

Date.prototype.getWeek = function() {

	var week = null;

	var target = new Date(this.valueOf()), dayNumber = (this.getDay() + 6) % 7, firstThursday;

	target.setDate(target.getDate() - dayNumber + 3);
	firstThursday = target.valueOf();
	target.setMonth(0, 1);

	if (target.getDay() !== 4) {
		target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
	}

	week = Math.ceil((firstThursday - target) / (7 * 24 * 3600 * 1000)) + 1;

	return week;
};

Date.prototype.isLeapYear = function() {

	var flag0 = null;
	var year = this.getFullYear();

	if ((year & 3) != 0) {
		flag0 = false;
	} else {
		flag0 = ((year % 100) != 0 || (year % 400) == 0) ? true : false;
	}

	return flag0;
};

Date.prototype.getDayOfYear = function() {
	var dayCount = [ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 ];
	var mn = this.getMonth();
	var dn = this.getDate();
	var dayOfYear = dayCount[mn] + dn;
	if (mn > 1 && this.isLeapYear())
		dayOfYear++;
	return dayOfYear;
};

Date.prototype.addMonths = function(value) {
	var n = this.getDate();
	this.setDate(1);
	this.setMonth(this.getMonth() + value);
	this.setDate(Math.min(n, this.getDaysOfMonth()));
	return this;
};

Date.prototype.isDayBetween = function(wd1, wd2) {

	wd1 = typeof +wd1 == "number" ? +wd1 : 0;
	wd2 = typeof +wd2 == "number" ? +wd2 : 0;

	var wd = this.getDay();
	var flag0 = false;

	var ini = wd1;
	var end = wd2;
	var dayOfWeek = wd;

	if (ini > end && (dayOfWeek >= ini || dayOfWeek <= end)) {
		flag0 = true;
	} else if (ini <= end && dayOfWeek >= ini && dayOfWeek <= end) {
		flag0 = true;
	}

	return flag0;
};

Date.prototype.getNumeric = function(only_date) {

	only_date = typeof only_date == "boolean" ? only_date : true;

	var year = String(this.getFullYear());
	var month = String(this.getMonth() + 1).zeroLeftPad();
	var day = String(this.getDate()).zeroLeftPad();
	var date = !isNaN(+(year + month + day)) ? String(year + month + day) : "";

	if (!only_date && date != "") {
		date += String(this.getHours()).zeroLeftPad()
				+ String(this.getMinutes()).zeroLeftPad()
				+ String(this.getSeconds()).zeroLeftPad();
	}
	return date != "" ? +date : 0;
};

Date.prototype.getDateISO8601 = function() {

	var year = String(this.getFullYear());
	var month = String(this.getMonth() + 1).zeroLeftPad();
	var day = String(this.getDate()).zeroLeftPad();

	return !isNaN(+(year + month + day)) ? String(year + '-' + month + '-'
			+ day) : "";
};

Date.prototype.setDateFromNumeric = function(numeric) {

	numeric = [ 'string', 'number' ].indexOf(typeof numeric) != -1 ? +numeric
			: 0;
	var matches = ("" + numeric).match(/(\d{4})+(\d{2})+(\d{2})/i);
	var data = [ 0, 0, 0 ];

	if (matches instanceof Array) {
		data[0] = matches[1];
		data[1] = matches[2];
		data[2] = matches[3];
	}

	this.setFullYear(data[0]);
	this.setMonth(data[1] - 1);
	this.setDate(data[2]);
};

module.exports = function(input_date, default_locale) {

	var obj = this;

	obj._date = null;

	/**
	 * Default locale
	 */
	obj._default_locale = 'en';

	obj._proto = {
		'locale' : {
			'D' : [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ],
			'l' : [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
					'Friday', 'Saturday' ],
			'F' : [ 'January', 'February', 'March', 'April', 'May', 'June',
					'July', 'August', 'September', 'October', 'November',
					'December' ],
			'M' : [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
					'Sept', 'Oct', 'Nov', 'Dec' ],
		}
	};

	obj.locale = {};

	obj.locale.es = {
		'D' : [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
		'l' : [ 'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes',
				'Sabado' ],
		'F' : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
				'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
		'M' : [ 'Ene', 'Feb', 'Mzo', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep',
				'Oct', 'Nov', 'Dic' ],
	};

	obj.locale.en = obj._proto.locale;

	obj.zeroLeftPad = function(n) {
		return String(n).zeroLeftPad();
	};

	/**
	 * Add days to the date.
	 * 
	 * @param int
	 *            n
	 * @return boolean
	 */
	obj.addDays = function(n) {
		obj.isValid() ? obj._date.setDate(obj._date.getDate() + n) : true;
		return obj.isValid();
	};

	/**
	 * Add monts to date
	 * 
	 * @param int
	 *            n
	 * @return boolean
	 */
	obj.addMonths = function(n) {
		// console.log('DateFormat addMonths',n);
		obj.isValid() ? obj._date.addMonths(n) : true;
		return obj.isValid();
	};

	/**
	 * Check if the year in the date is leap o not
	 * 
	 * @return boolean
	 */
	obj.isLeapYear = function() {

		return obj.isValid() ? obj._date.isLeapYear() : null;
	};

	/**
	 * Get the day of the year
	 * 
	 * @return int
	 */
	obj.getDayOfYear = function() {
		return obj.isValid() ? obj._date.getDayOfYear() : null;
	};

	/**
	 * Get the week of the year
	 * 
	 * @return int
	 */
	obj.getWeek = function() {
		return obj.isValid() ? obj._date.getWeek() : null;
	};

	/**
	 * Get the days of the current month.
	 * 
	 * @return int
	 */
	obj.getDaysOfMonth = function() {
		return obj.isValid() ? obj._date.getDaysOfMonth() : null;
	};

	/**
	 * List of the PHP formats...
	 */
	obj._formats = {
		'd' : function(locale) {
			return obj.isValid() ? obj.zeroLeftPad(obj._date.getDate()) : "";
		},
		'D' : function(locale) {
			return obj.isValid() && typeof locale.D != "undefined" ? locale.D[obj._date
					.getDay()]
					: "";
		},
		'j' : function(locale) {
			return obj.isValid() ? obj._date.getDate() : "";
		},
		'l' : function(locale) {
			return obj.isValid() && typeof locale.l != "undefined" ? locale.l[obj._date
					.getDay()]
					: "";
		},
		'N' : function(locale) {
			return obj.isValid() ? (obj._date.getDay() > 0 ? obj._date.getDay()
					: 7) : "";
		},
		'w' : function(locale) {
			return obj.isValid() ? obj._date.getDay() : "";
		},
		'z' : function(locale) {
			return obj.isValid() ? obj.getDayOfYear() : "";
		},
		'W' : function(locale) {
			return obj.isValid() ? obj.getWeek() : "";
		},
		'F' : function(locale) {
			return obj.isValid() && typeof locale.F != "undefined" ? locale.F[obj._date
					.getMonth()]
					: "";
		},
		'm' : function(locale) {
			return obj.isValid() ? obj.zeroLeftPad(obj._date.getMonth() + 1)
					: "";
		},
		'M' : function(locale) {
			return obj.isValid() && typeof locale.M != "undefined" ? locale.M[obj._date
					.getMonth()]
					: "";
		},
		'n' : function(locale) {
			return obj.isValid() ? obj._date.getMonth() + 1 : "";
		},
		't' : function(locale) {
			return obj.isValid() ? obj.getDaysOfMonth() : "";
		},
		'L' : function(locale) {
			return obj.isValid() ? +obj.isLeapYear() : "";
		},
		'o' : function(locale) {

			var target = obj.isValid() ? new Date(obj._date.valueOf()) : null;
			var week = target.getWeek();
			var year = target.getFullYear();
			target.addDays(7);
			var week_next = target.getWeek();
			return obj.isValid() ? (week > week_next ? year - 1 : year) : "";
		},
		'Y' : function(locale) {
			return obj.isValid() ? obj._date.getFullYear() : "";
		},
		'y' : function(locale) {
			var year = obj.isValid() ? String(obj._date.getFullYear()) : "";
			return obj.isValid() ? year.substr(year.length - 2, 2) : "";
		},
		'a' : function(locale) {
			return obj.isValid() ? (obj._date.getHours() > 12 ? "pm" : "am")
					: "";
		},
		'A' : function(locale) {
			return String(obj._formats.a(locale)).toUpperCase();
		},
		'g' : function(locale) {
			return obj.isValid() ? +(obj._date.getHours() % 12) : "";
		},
		'G' : function(locale) {
			return obj.isValid() ? +obj._date.getHours() : "";
		},
		'h' : function(locale) {
			return obj.zeroLeftPad(obj._formats.g(locale));
		},
		'H' : function(locale) {
			return obj.zeroLeftPad(obj._formats.G(locale));
		},
		'i' : function(locale) {
			return obj.isValid() ? obj.zeroLeftPad(obj._date.getMinutes()) : "";
		},
		's' : function(locale) {
			return obj.isValid() ? obj.zeroLeftPad(obj._date.getSeconds()) : "";
		},
	};

	/**
	 * Check if the object is a valid one (date)
	 * 
	 * @return boolean
	 */
	obj.isValid = function() {
		return obj.isValidDate(obj._date);
	};

	/**
	 * Check if the input yyyy-mm-dd is a valid one
	 * 
	 * @param string
	 * @return boolean
	 */
	obj.isValidDate = function(input_date) {

		var flag0 = typeof input_date == "object" && input_date instanceof Date ? true
				: false;
		flag0 = flag0 && obj._date.isValid() ? true : flag0;

		return flag0;
	};

	/**
	 * convert a string YYYY-MM-DD HH:ii:SS to a js Date object if the provided
	 * date is valid.
	 * 
	 * @param string
	 * @return Date / null
	 */
	obj.stringToDate = function(string) {

		string = typeof string == "string" ? string : "";

		var array_date = string.split(" ");

		var dp = /^(\d{4})[\-\.]{1}(\d{1,2})[\-\.]{1}(\d{1,2})/ig;
		var matches = dp.exec(array_date[0]);
		var matches_hour = [];
		var _date = null;

		if (typeof array_date[1] == "string") {
			matches_hour = array_date[1].split(":");
		}

		for (var i = 0; i < 3; i++) {
			matches_hour[i] = typeof matches_hour[i] != "undefined" ? +matches_hour[i]
					: 0;
		}

		// console.log(matches);

		if (typeof matches == "object" && matches instanceof Array) {

			_date = new Date(+matches[1], +matches[2] - 1, +matches[3],
					matches_hour[0], matches_hour[1], matches_hour[2]);

			// -validando y llevando a nulo!
			if (_date.getFullYear() != +matches[1]
					|| _date.getMonth() != +matches[2] - 1
					|| _date.getDate() != +matches[3]) {
				_date = null;
			}
		}

		return _date;
	};

	/**
	 * Constructor of the object
	 * 
	 * @param string
	 *            input_date
	 * @param string
	 *            default_locale
	 */
	obj.init = function(input_date, default_locale) {

		var tmp = typeof input_date == "object" && input_date instanceof Date ? input_date
				: false;
		tmp = tmp == false ? obj.stringToDate(input_date) : tmp;
		obj._date = typeof tmp == "object" && tmp instanceof Date ? tmp : false;
		obj._default_locale = obj.isLocale(default_locale) ? default_locale
				: obj._default_locale;
	};

	/**
	 * Check if provided locale exists
	 * 
	 * @param string
	 *            lang
	 * @return boolean
	 */
	obj.isLocale = function(lang) {
		return typeof lang == "string" && typeof obj.locale[lang] == "object" ? true
				: false;
	};

	/**
	 * Get locale settings
	 * 
	 * @param string
	 *            lang
	 * @return object
	 */
	obj.getLocale = function(lang) {
		return typeof obj.locale[lang] == "object" ? obj.locale[lang]
				: obj.locale[obj._default_locale];
	};

	/**
	 * Gets a string representation of date. Using the PHP date format. 
	 * If the current date is not valid returns empty string
	 * 
	 * 
	 * @param string input_format
	 * @param string lang
	 * @return string
	 */
	obj.format = function(input_format, lang) {

		var locale = obj.getLocale(lang);

		input_format = typeof input_format == "string" ? input_format : "";
		var string_date_format = input_format;

		var mp = /\%([\w]{1,2})/ig;
		var matches = null;

		do {

			matches = mp.exec(input_format);

			// -aqui capturo las equivalencias...
			if (matches instanceof Array && matches[1] != "undefined") {
				// console.log(matches);
				var search = matches[0];
				var pattern = matches[1];
				var replace = typeof obj._formats[pattern] == "function" ? obj._formats[pattern]
						(locale)
						: "";
				string_date_format = string_date_format
						.replace(search, replace);
			}

		} while (matches !== null);

		return string_date_format;
	};

	/**
	 * get a number representing the date YYYYmmdd or datetime YYYYmmddhhmmss
	 * 
	 * @param boolean
	 *            solo_fecha
	 * @return int
	 */
	obj.getNumeric = function(solo_fecha) {
		return obj.isValid() ? obj._date.getNumeric(solo_fecha) : 0;
	};

	/**
	 * Returns a YYYY-mm-dd string of the current date
	 * 
	 * @return string
	 */
	obj.getDateISO8601 = function() {
		return obj.isValid() ? obj._date.getDateISO8601() : "";
	};

	/**
	 * Check if the current weekday(0-6) is between w1(0-6) and w2 (0-6)
	 * 
	 * @param int
	 *            wd1
	 * @param int
	 *            wd2
	 * @return boolean
	 */
	obj.isDayBetween = function(wd1, wd2) {
		return obj.isValid() ? obj._date.isDayBetween(wd1, wd2) : undefined;
	};

	obj.init(input_date, default_locale);

	return obj;
};
